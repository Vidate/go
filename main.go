package main

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"os"
	"strings"
	"time"
)

func main2() {

	dat, _ := ioutil.ReadFile("test.csv")
	timer := time.NewTimer(time.Second * 30)
	stop := false

	go func() {
		<-timer.C
		stop = true
		fmt.Println("Timer 2 expired")
	}()

	for _, element := range strings.Split(string(dat), "\r\n") {

		data := strings.Split(element, ",")
		question := data[0]
		correctAnswer := string(data[1])

		fmt.Print(question)
		reader := bufio.NewReader(os.Stdin)
		fmt.Print("Pleas write your anwser: ")
		text, _, _ := reader.ReadLine()

		answer := string(text)

		if answer == correctAnswer {
			fmt.Println("dobrze")
		}
		if stop {
			break
		}
	}
}
